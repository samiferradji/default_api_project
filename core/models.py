from django.db import models


class Product(models.Model):
    """
    A simple product model
    """
    designation = models.CharField(
        max_length=200,
        verbose_name="La Désignation",
        help_text="Nom commercial du produit "
    )
    is_active = models.BooleanField(
        verbose_name="Est Actif?",
        help_text="Oui, si le prduit peut être mouvementé"
    )
    selling_price = models.DecimalField(
        max_digits=12, decimal_places=2,
        verbose_name="Prix de vente",
        help_text="le prix de vente proposé au clients"

    )

    def __str__(self):
        return self.designation