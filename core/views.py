from rest_framework import viewsets
from rest_framework import routers


from core.models import Product
from core.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


# Register viewsets

router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
